'use client';

import { useAuth } from '../app/context/context'
import Link from 'next/link';

export default function navBar()
{
    const { isLoggedIn, login, logout } = useAuth();

    return (
        <div className="flex justify-between pl-12 pr-12 py-3 text-xl font-semibold bg-gradient-to bg-gradient-to-b from-indigo-300">
            <Link href={'/'} className="z-10 hover:text-purple-500">Home</Link>
            <nav className="z-10 flex gap-12">
                {isLoggedIn ? 
                <>
                <Link href={'/dashboard'} className="hover:text-purple-500">Dashboard</Link>
                <Link href={'/'} onClick={logout} className="hover:text-purple-500">Log Out</Link>
                </> : 
                <Link href={'/login'} className="hover:text-purple-500">Log In</Link>}
            </nav>
        </div>
    )
}