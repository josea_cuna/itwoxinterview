'use client';
import Link from "next/link";
import { useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faComments } from '@fortawesome/free-regular-svg-icons'

type Post = {
    id: number;
    title: string;
    body: string;
}

type Comment = {
    postId: number;
    name: string;
    email: string;
    body: string;
  }
  

export default function Posts(props: {
    posts: Post[];
    comments: Comment[];
}){
    const [currentPage, setCurrentPage] = useState(0);
    const [totalPages, setTotalPages] = useState(Math.ceil(props.posts.length / 10) - 1);

    const handleNextPage = () => {
        if (currentPage < totalPages) {
            setCurrentPage(currentPage => currentPage + 1);
        }
    }

    const handlePreviousPage = () => {
        if (currentPage > 0) {
            setCurrentPage(currentPage => currentPage - 1);
        }
    }

    const currentPosts = props.posts.slice(currentPage * 10, (currentPage + 1) * 10);

    // because of the comments being a separate array from posts, we need to iterate over it to get the amount of 
    // comments for each post. This is not too bad because of the pagination, we only calculate the amount of comments
    // for the 10 posts that are currently being displayed.
    const getCommentCount = (postId: number) => {
        return props.comments.filter((comment: Comment) => comment.postId === postId).length;
      };

    return (
        <div className="flex flex-col self-center w-3/5 gap-4">
            <div className="flex flex-col gap-1" >
            {currentPosts.map((post: Post) => (
                        <Link href={`/dashboard/${post.id}`} key={post.id} className="flex justify-between bg-indigo-300 p-3 rounded-xl text-sm hover:bg-indigo-500">
                            <h2 data-testid={'postTitle'}>{post.title}</h2>
                            <div className="flex gap-2 items-center">
                            <p data-testid={'commentCount'} >{getCommentCount(post.id)}</p>
                            <FontAwesomeIcon icon={faComments} />
                            </div>
                        </Link>
                    ))}
            </div>
            <div className="self-end flex items-center gap-2 text-sm">
                <button className="btn btn-primary bg-purple-300 hover:bg-purple-500 p-2 rounded-xl" onClick={handlePreviousPage}>Previous</button>
                <p>{`Page: ${currentPage+1}`}</p>
                <button className="btn btn-primary bg-purple-300 hover:bg-purple-500 p-2 rounded-xl" onClick={handleNextPage}>Next</button>
            </div>
        </div>
    )
}