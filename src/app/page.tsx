import Image from 'next/image'
import NavBar from '../components/NavBar.component'


export default function Home() {

  return (
    <>
    <NavBar />
    <main className="flex min-h-screen flex-col items-center justify-between pt-5">
        <h1 className="text-4xl font-bold">Welcome</h1>
    </main>
    </>
  )
}
