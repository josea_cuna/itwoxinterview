// it needs to be a client component because its using useState
'use client';

import React, { createContext, useContext, useState, useEffect, ReactNode } from 'react';

type authContextType = {
    isLoggedIn: boolean;
    login: () => void;
    logout: () => void;
};

const authContextDefaultValues: authContextType = {
    isLoggedIn: false,
    login: () => {},
    logout: () => {},
};

const AuthContext = createContext<authContextType>(authContextDefaultValues);

export function useAuth() {
    return useContext(AuthContext);
}

type Props = {
    children: ReactNode;
};

export function AuthProvider({ children }: Props) {
    const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false);

    useEffect(() => {
        const storedIsLoggedIn = localStorage.getItem('isLoggedIn');
        if (storedIsLoggedIn) {
          setIsLoggedIn(JSON.parse(storedIsLoggedIn));
        }
    }, [])

    const login = () => {
        setIsLoggedIn(true);
        localStorage.setItem('isLoggedIn', JSON.stringify(true));
    };

    const logout = () => {
        setIsLoggedIn(false);
        localStorage.setItem('isLoggedIn', JSON.stringify(false));
    };

    const value = {
        isLoggedIn,
        login,
        logout,
    };

    return (
        <>
            <AuthContext.Provider value={value}>
                {children}
            </AuthContext.Provider>
        </>
    );
}