import NavBar from "@/components/NavBar.component"
import Posts from "@/components/Posts.component";
import Table from "@/components/Posts.component";

type Post = {
  id: number;
  title: string;
  body: string;
}

type Comment = {
  postId: number;
  name: string;
  email: string;
  body: string;
}

export default async function dashboard() {
  // fetch posts. Since I'm using nextjs 13, I can call the api from here because this is by default a React server component.
  // this will be the equivalent to getClientSideProps in past versions of next.
  // See https://nextjs.org/docs/app/building-your-application/data-fetching
  const fetchPosts = async () => {
    try {
      const response = await fetch(`${process.env.POSTS_API}`);
      const data = await response.json();
      return data;
    } catch (error) {
      console.error('Error fetching posts:', error);
    }
  };

  const posts: Post[] = await fetchPosts();

  // fetch comments. Same as above. Both are called from the server when built instead of on each re-render as with client components,
  // so this is better for performance
  const fetchComments = async () => {
    try {
      const response = await fetch(`${process.env.COMMENTS_API}`);
      const data = await response.json();
      return data;
    } catch (error) {
      console.error('Error fetching comments:', error);
    }
  };

  const comments: Comment[] = await fetchComments()
  
  return (
    <>
    <NavBar />
    <main className="flex min-h-screen flex-col items-stretch gap-5 pt-5">
      <h1 className="self-center text-xl font-bold">Dashboard</h1>
      <Posts posts={posts} comments={comments}></Posts>
    </main>
    </>
  )
}
