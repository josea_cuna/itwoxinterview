import NavBar from "@/components/NavBar.component"

type Post = {
    id: number;
    title: string;
    body: string;
}

type Comment = {
    postId: number;
    id: number;
    name: string;
    email: string;
    body: string;
  }

export default async function posts({params}: {
    params: {postId: string}
}) {

    const fetchPost = async () => {
        try {
          const response = await fetch(`${process.env.POSTS_API}${params.postId}`);
          const data = await response.json();
          return data;
        } catch (error) {
          console.error('Error fetching post:', error);
        }
    };

    const post: Post = await fetchPost();

    const fetchComments = async () => {
        try {
            const response = await fetch(`${process.env.COMMENTS_API}?postId=${params.postId}`);
            const data = await response.json();
            return data;
        } catch (error) {
        console.error('Error fetching comments:', error);
        }
    }

    const comments = await fetchComments();

    return (
        <>
        <NavBar />
        <main className="flex min-h-screen flex-col items-center gap-5 rounded-xl pt-5 mx-10">
            <h1 className="font-bold text-xl w-3/5">{post.title}</h1>
            <p className="text-lg w-3/5">{post.body}</p>
            <h2 className="text-lg font-semibold w-3/5">Comments</h2>
            <div className="flex flex-col items-center gap-3 w-3/5">
            {comments.map((comment: Comment) => (
                <div className="w-full bg-gray-300 p-5 rounded-xl" key={comment.id}>
                    <p className="font-semibold">{comment.email}</p>
                    <p >{comment.name}</p>
                    <p className="text-sm" >{comment.body}</p>
                </div>
            ))}
            </div>
        </main> 
        </>
    )
} 