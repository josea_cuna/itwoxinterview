'use client';
import { useRouter } from 'next/navigation';
import { FormEvent, useState } from 'react';
import { useAuth } from '../context/context'

export default function login() {
    const { login } = useAuth();
    const router = useRouter();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleLogin = async (event : FormEvent<HTMLElement>) => {
        event.preventDefault();
        console.log(email, password)
        // here I would handle the auth, but since it's not specified in the test, I'm gonna ommit it, if I
        // where to implement it I would use a provider like AuthO, Firebase or Clerk.

        try {
            login();
            router.push('/dashboard');
        } catch (error) {
            alert(error);
        }
    }

    return ( 
        <main className="flex min-h-screen flex-col items-center mt-20">
            <div className="flex flex-col items-center gap-5 p-5 w-2/5 bg-indigo-300 rounded-xl">  
                <h1 className="text-xl font-bold">Login</h1>
                <form className="flex flex-col gap-3 w-4/5" onSubmit={handleLogin}>
                    <input className="rounded-sm" type="email" name="email" placeholder="  Email" onChange={(event) => setEmail(event.target.value)} required/>
                    <input className="rounded-sm" type="password" name="password" placeholder="  Password" onChange={(event) => setPassword(event.target.value)} required/>
                    <button type="submit" className="bg-purple-300 p-1 rounded-xl hover:bg-purple-500">Enter</button>
                </form>
            </div>
        </main>
    )
}