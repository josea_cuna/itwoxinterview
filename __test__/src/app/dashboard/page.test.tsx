import React from "react";
import { render, screen } from "@testing-library/react";
import '@testing-library/jest-dom';
import Posts from "@/components/Posts.component";

describe('Dashboard posts page', () => {
    it('Should render the comment title correctly', () => {
        const posts = [{id: 1, title: 'test title', body: 'test body'}]
        const comments = [{postId: 1, name: 'test name 1', email: 'test email 1', body: 'test body 1'}, {postId: 1, name: 'test name 2', email: 'test email 2', body: 'test body 2'}]
        render(<Posts posts={posts} comments={comments}/>)

        const commentAmount = screen.getByTestId('postTitle').innerHTML;
        expect(commentAmount).toBe(posts[0].title)
    })

    it('Should render the comment amount correctly', () => {
        const posts = [{id: 1, title: 'test title', body: 'test body'}]
        const comments = [{postId: 1, name: 'test name 1', email: 'test email 1', body: 'test body 1'}, {postId: 1, name: 'test name 2', email: 'test email 2', body: 'test body 2'}]
        render(<Posts posts={posts} comments={comments}/>)

        const commentAmount = screen.getByTestId('commentCount').innerHTML;
        expect(commentAmount).toBe(comments.length.toString())
    })
})