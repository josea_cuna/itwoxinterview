## Overview of the solution

## Running locally
- run `npm install`
- run `npm run dev`
- access it by going to [localhost:3000](localhost:3000)

## Stack
- I used Nextjs13 to wrap the React project, so I implemented some of their latests features like the app router, layouts and react server components. See [https://nextjs.org/docs/getting-started/project-structure](Next Docs) to learn more about the features.
- The project folder structure goes like this:
    - src (top directory of the code)
        - app (where the pages are)
            - context (where the context is)
            - dashboard (dashboard page)
                - [postId] (individual post page, see dynamic routing in Next Doc)
            - login (login page)
            - page.tsx (home page code, entry point)
        - components (client components used by the pages)
- For the style, I use Tailwind.
- To share the state and maintain it across the pages I use React's Context.
- I used the react testing library + jest for the testing.

## Areas of improvement in case of having more time
- Testing: I would like to increase the coverage of the tests for the project
- UI: Even though it is responsive now, I would like to implement more screen use-cases
- Auth: I would like to handle user authentification using an Auth provider to benefit from the existing robust technologies
